<?php

$request_uri = parse_url($_SERVER['REQUEST_URI']);

if($request_uri['path'] == '/'){
    $uri = "Senepol\\Api\\" . HOME;
    $control = new $uri;
    $control->index($_REQUEST); 
    exit;
}

$uriArray = explode("/", $request_uri['path']);

if(isset($uriArray[0])){
    $controller = "Senepol\\Api\\" . ucfirst($uriArray[1]).'Controller';
    $method = isset($uriArray[2]) ? $uriArray[2] : 'index';
    $parameter = isset($uriArray[3]) ? $uriArray[3] : null;

    if(class_exists($controller)) {
        $control = new $controller;
        $_REQUEST['parameter'] = $parameter;
        $control->{$method}($_REQUEST); 
    }else {
        header("Location: /error/e404");
    }

}else{
    header("Location: /error/e404");
}
