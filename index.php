<?php
/**
 * SENEPOL Inteligent System for Web Development
 * by @mriso79
 * @since 2021-13-04
 */
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/constants.php';
require __DIR__ . '/config.php';