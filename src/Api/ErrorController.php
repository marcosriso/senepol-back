<?php
namespace Senepol\Api;
use Senepol\Lib\Interfaces\iErrorInterface;
use Senepol\Lib\Util\JSONresponseUtil;

class ErrorController implements iErrorInterface {

    public function e404($request){
        JSONresponseUtil::respond(['response'=>'Page not found']);
    }

}