<?php
namespace Senepol\Api;
use Senepol\Lib\Interfaces\iControllerInterface;
use Senepol\Lib\Util\JSONresponseUtil;

class HomeController implements iControllerInterface {

    public function index($request){
        JSONresponseUtil::respond(['response'=>'All fine with home controller']);
    }

    public function getdata($request){
        JSONresponseUtil::respond(['response'=>'All fine with getdata method', 'parameter'=>$request['parameter']]);
    }

}