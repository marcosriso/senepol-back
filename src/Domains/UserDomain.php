<?php
namespace Senepol\Domains;

class UserDomain {

    private $id;
    private $name;
    private $email;
    private $password;

    function __construct($id, $name, $email, $password) {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }
}