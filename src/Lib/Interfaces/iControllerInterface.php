<?php
namespace Senepol\Lib\Interfaces;

interface iControllerInterface {
    public function index($request);
}