<?php
namespace Senepol\Lib\Interfaces;

interface iErrorInterface {
    public function e404($request);
}