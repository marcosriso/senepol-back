<?php
namespace Senepol\Lib\Util;

class JSONresponseUtil {

    public static function respond($array){
        header('Content-Type: application/json');
        echo json_encode($array);
    }

}